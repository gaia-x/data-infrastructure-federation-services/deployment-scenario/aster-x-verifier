import { Controller, Get, Param, Post, Body, HttpCode, Query } from '@nestjs/common';
import { AppService } from './app.service';
import { ApiTags, ApiResponse, ApiOperation,  ApiParam, ApiBody} from '@nestjs/swagger';
import {
  CredentialSubjectDto,
  RulesDescription, testResult, VerifiableCredentialDto, ID, IDld
} from './dto'
import * as payload from './static/example.json'
import { PayloadParser } from './pipes/parser.pipe';
let bodyExample:(ID | VerifiableCredentialDto<CredentialSubjectDto> | IDld)[] = payload


@ApiTags("Verifier ")
@Controller({ path: '/api/' })
export class AppController {
  constructor(private readonly appService: AppService ) {}

  @ApiResponse({
    status: 200,
    description: 'A list of all supported rules and their description'
  })
  @ApiOperation({
    summary:
      'Search for all the rules supported in the Verifier'
  })
  @Get("/rules")
  getRules(): RulesDescription[] {
    return this.appService.getAllRules();
  }

  @ApiResponse({
    status: 200,
    description: 'The rule searched and its description'
  })
  @ApiResponse({
    status: 404,
    description: 'Rules not found in list of supported rules'
  })
  @ApiOperation({
    summary:'Search for a specific rule in this abc-checker version'
  })
  @ApiParam({
    name:"id",
    required:true,
    type: 'string',
    description: 'ID of the rules searched',
    example:"rule_01"
  })
  @Get("/rules/:id")
  getRule(@Param("id") id):RulesDescription {
    return this.appService.getRules(id)
  }




  @ApiResponse({
    status: 200,
    description: 'The VCs has passed all the tests'
  })
  @ApiResponse({
    status: 403,
    description: 'One or more test has failed '
  })
  @ApiOperation({
    summary:'Apply a certain amount of checks to a set of VCs / id'
  })
  @ApiBody({
    type: 'array',
    examples: {
      example: {
        value:bodyExample
      }
    }
  })
  @HttpCode(200)
  @Post("/rules")
  async checkRule(@Body() payload: (ID | VerifiableCredentialDto<CredentialSubjectDto> | IDld)[]):Promise<any> {
    let parserPipe = new PayloadParser(this.appService)
    let parsedPayload = await parserPipe.transform(payload)
    let result = await this.appService.checkRules(parsedPayload)
    return result
  }

  @ApiResponse({
    status: 200,
    description: 'The id is resolvable'
  })
  @ApiResponse({
    status: 404,
    description: 'The id is incorrect'
  })
  @ApiOperation({
    summary:'Resolve an id, whether its a did or a http uri'
  })
  @ApiParam({
    example:"did:web:abc",
    name:"id"
  })
  @HttpCode(200)
  @Get("/resolve/:id")
  async resolve(@Param() param) {
    
   
    return  await this.appService.resolveURI(param.id)
  }

  
}
