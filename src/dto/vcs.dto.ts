import { ApiProperty } from '@nestjs/swagger'
import { CredentialSubjectDto, VerifiableCredentialDto } from './vc.dto'

export abstract class VerificationPayload {

  @ApiProperty({
    description: 'Array of credential'
  })
  public vp: any

  @ApiProperty({
    description: 'Array of credential'
  })
  public rules: string[]


}


