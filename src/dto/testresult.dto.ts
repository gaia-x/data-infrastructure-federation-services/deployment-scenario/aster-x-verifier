import { ApiProperty } from '@nestjs/swagger'

export class testResult {
  @ApiProperty({
    description: 'Id of the vc tested'
  })
  vc_id:string

  @ApiProperty({
    description: 'Status of the verification'
  })
  is_valid: boolean

  @ApiProperty({
    description: "Error details"
  })
  invalidRules?: testError[]

  @ApiProperty({
    description: "Error details"
  })
  warning?: testError[]

}

export class opaResult {
  @ApiProperty({
    description: 'Id of the rule'
  })
  rule_id: string

  @ApiProperty({
    description: 'Error message'
  })
  message: string

  @ApiProperty({
    description: 'Error message'
  })
  result: boolean | any
}


class testError {
  @ApiProperty({
    description: "Error message"
  })
  message:string[]
}
