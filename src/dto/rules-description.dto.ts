import { ApiProperty } from '@nestjs/swagger'

export class RulesDescription {
  @ApiProperty({
    description: 'Id of the rule'
  })
  id: string

  @ApiProperty({
    description: 'description of the rule'
  })
  description: string
  @ApiProperty({
    description: 'description of the rule'
  })
  subsequentRule?:SubSequentRule[]
  @ApiProperty({
    description: 'message Displayed if the test is not concluent'
  })
  warning?:string
  error?:string
}


 class SubSequentRule {

  @ApiProperty({
    description: 'Id of the rule'
  })
  id: string

  @ApiProperty({
    description: 'message Displayed if the test is not concluent'
  })
  error: string

  @ApiProperty({
    description: 'description of the rule'
  })
  description: string



}



