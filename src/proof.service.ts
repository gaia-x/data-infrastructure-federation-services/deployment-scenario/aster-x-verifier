import  { createHash } from 'crypto'
import * as jose from 'jose'
import * as jsonld from 'jsonld'
import { DIDDocument, Resolver } from 'did-resolver'
import * as web from 'web-did-resolver'
import { BadRequestException, ConflictException, Injectable, Logger } from '@nestjs/common'
import { testResult } from './dto'
import { opaResult } from './dto'


const webResolver = web.getResolver()
const resolver = new Resolver(webResolver)
const logger = new Logger('Signature service')

export class ProofService {


    public async validate(
        selfDescriptionCredential: any
      ): Promise<opaResult> {
        try {
          const { x5u, publicKeyJwk } = await this.getPublicKeys(selfDescriptionCredential)
          const isValidSignature: any = await this.checkSignature(selfDescriptionCredential, selfDescriptionCredential.proof, publicKeyJwk)
          return isValidSignature
        } catch(e) {
          return {
            message:e.response.message,
            result: false,
            rule_id: 'Signature Rule'
          }
        }
      }

    private async checkSignature(selfDescription, proof, jwk: any): Promise<opaResult> {
        try {
          console.log(jwk)
          const clonedSD = this.clone(selfDescription)
          delete clonedSD.proof
          const normalizedSD: string = await this.normalize(clonedSD)
          const hash: string = this.sha256(normalizedSD)
          const verificationResult: Verification = await this.verify(proof?.jws.replace('..', `.${hash}.`), jwk)
          if(verificationResult.content === hash) {
            return {
                message:"Signature is correct",
                result: true,
                rule_id: 'Signature Rule'
              }
          }
        } catch (e) {
          try {
            {
              console.log(selfDescription.proof)
              logger.log('Beginning Waltid signature verification')
              const clonedSD = this.clone(selfDescription)
              const proof_copy = { ...selfDescription.proof }
              delete clonedSD.proof
              delete proof_copy.jws
              proof_copy['@context'] = selfDescription['@context']
              const normalizedCredential: string = await this.normalize(clonedSD)
              const normalizedProof = await this.normalize(proof_copy)
              const hash = new Uint8Array(64)
              hash.set(this.sha256_bytes(normalizedProof))
              hash.set(this.sha256_bytes(normalizedCredential), 32)
              const verificationResult = await this.verify_walt(proof.jws, jwk, hash)
              if( Buffer.from(verificationResult.content).toString('hex') === Buffer.from(hash).toString('hex')) {
                return {
                    message:"Signature is correct",
                    result: true,
                    rule_id: 'Signature Rule'
                  }
              }
            }
          } catch (e) {
            console.error(e)
            return {
                message:"Signature is incorrect",
                result: false,
                rule_id: 'Signature Rule'
              }
            
          }
        }
      }

      async verify_walt(jws: any, jwk: any, hash: Uint8Array) {
        try {
          const cleanJwk = {
            kty: jwk.kty,
            n: jwk.n,
            e: jwk.e,
            x5u: jwk.x5u
          }
          const splited = jws.split('.')
          const algorithm = jwk.alg || 'PS256'
          const rsaPublicKey = (await jose.importJWK(cleanJwk, algorithm)) as jose.KeyLike
    
          const result = await jose.flattenedVerify(
            {
              protected: splited[0],
              signature: splited[2],
              payload: hash
            },
            rsaPublicKey
          )
          return { protectedHeader: result.protectedHeader, content: result.payload }
        } catch (error) {
          throw new ConflictException('Verification for the given jwk and jws failed.')
        }
      }

      getId(selfDescription:any) {
        if(selfDescription.id) {
            return selfDescription.id
        } 
        if(selfDescription["@id"]) {
            return selfDescription["@id"]
        }
      }

      private async loadDDO(did: string): Promise<any> {
        let didDocument
        try {
          didDocument = await this.getDidWebDocument(did)
        } catch (error) {
          throw new ConflictException(`Could not load document for given did:web: "${did}"`)
        }
        if (!didDocument?.verificationMethod || didDocument?.verificationMethod?.constructor !== Array)
          throw new ConflictException(`Could not load verificationMethods in did document at ${didDocument?.verificationMethod}`)
    
        return didDocument || undefined
      }

      private async getDidWebDocument(did: string): Promise<DIDDocument> {
        const doc = await resolver.resolve(did)
    
        return doc.didDocument
      }

      public async getPublicKeys(selfDescriptionCredential) {
        try {
          if (!selfDescriptionCredential || !selfDescriptionCredential.proof) {
            throw new ConflictException('Missing field: Proof in the Verifiable Credenial')
          }
  
          const { verificationMethod, id } = await this.loadDDO(selfDescriptionCredential.proof.verificationMethod)
      
          const jwk = verificationMethod.find(method =>  method.id.startsWith(id))
          if (!jwk) throw new ConflictException(`Missing field: Verification Method in the Verifiable Credenial`)
      
          const { publicKeyJwk } = jwk
          if (!publicKeyJwk) throw new ConflictException(`Could not load JWK for ${verificationMethod}`)
      
          const { x5u } = publicKeyJwk
          if (!publicKeyJwk.x5u) throw new ConflictException(`The x5u parameter is expected to be set in the JWK for ${verificationMethod}`)
      
          return { x5u, publicKeyJwk }
        } catch(e) {
          throw(e)
        }

      }

      async normalize(doc: object): Promise<string> {
        const normalizationObject = doc
        const W3Context = 'https://www.w3.org/2018/credentials/v1'
        const SigContext = 'https://w3id.org/security/suites/jws-2020/v1'
        normalizationObject['@context'] = normalizationObject['@context'].map(x => {
          if (x.includes(W3Context)) {
            return x.replace(W3Context, 'https://schema-registry.aster-x.demo23.gxfs.fr/contexts/credentials')
          } else if (x.includes(SigContext)) {
            return x.replace(SigContext, 'https://schema-registry.aster-x.demo23.gxfs.fr/contexts/jws-2020')
          } else return x
        })
        let canonized: string
        try {
          canonized = await jsonld.canonize(normalizationObject, {
            algorithm: 'URDNA2015',
            format: 'application/n-quads',
            safe: false,
            base: null
          })
        } catch (error) {
          throw new BadRequestException('Provided input is not a valid Self Description.', error.message)
        }
        if ('' === canonized) {
          throw new BadRequestException('Provided input is not a valid Self Description.', 'Canonized SD is empty')
        }
        return canonized
      }
    

      private clone(objectToClone) {
        return JSON.parse(JSON.stringify(objectToClone))
      }

      sha256(input: string): string {
        return createHash('sha256').update(input).digest('hex')
      }
    
      sha256_bytes(input: string): Uint8Array {
        return createHash('sha256').update(input).digest()
      }


      async verify(jws: any, jwk: any): Promise<Verification> {
        try {
          const cleanJwk = {
            kty: jwk.kty,
            n: jwk.n,
            e: jwk.e,
            x5u: jwk.x5u
          }
          const algorithm = jwk.alg || 'PS256'
          const rsaPublicKey = await jose.importJWK(cleanJwk, algorithm)
    
          const result = await jose.compactVerify(jws, rsaPublicKey)
    
          return { protectedHeader: result.protectedHeader, content: new TextDecoder().decode(result.payload) }
        } catch (error) {
          throw new ConflictException('Verification for the given jwk and jws failed.')
        }
      }
}


export interface Verification {
    protectedHeader: jose.CompactJWSHeaderParameters | undefined
    content: string | undefined
  }

