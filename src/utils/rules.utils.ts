import { readFileSync } from 'fs'

export function get_rules() {
    let path = process.env.RULES_PATH
    const rules = readFileSync(path + `/rules.json`, 'utf-8')
    return JSON.parse(rules)
}


export function get_all_rules() {
    let path = process.env.RULES_PATH

    const rules = JSON.parse(readFileSync(path + `/rules.json`, 'utf-8'))

    return rules
}