import { CredentialSubjectDto, VerifiableCredentialDto } from "../dto"

export function getId(vc: VerifiableCredentialDto<CredentialSubjectDto>) {
    if(vc.id) {
        return vc.id
    } else {
        return vc["@id"]
    }
}