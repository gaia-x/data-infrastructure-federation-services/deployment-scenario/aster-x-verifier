import { PipeTransform, Injectable, UnprocessableEntityException, Logger } from '@nestjs/common'
import {  VerifiableCredentialDto, CredentialSubjectDto, ID, IDld} from '../dto'
import { AppService } from '../app.service'
@Injectable()
export class PayloadParser implements PipeTransform {
  constructor(private service: AppService) {}
  logger = new Logger("Parser Pipe")
  async transform(payload:(ID | VerifiableCredentialDto<CredentialSubjectDto> | IDld)[]): Promise<VerifiableCredentialDto<CredentialSubjectDto>[]> {
    try {
      let transformedPayload:VerifiableCredentialDto<CredentialSubjectDto>[] = []
      let unParsedIDs: string[] = []
      let resolutionPromises:Promise<any>[] = []
      payload.map(element => {
        if((element as VerifiableCredentialDto<CredentialSubjectDto>).proof && (element as VerifiableCredentialDto<CredentialSubjectDto>).credentialSubject) {
          transformedPayload.push(element as VerifiableCredentialDto<CredentialSubjectDto>)
        } else if(Object.keys(element).length===1 && element["id"]) {
          unParsedIDs.push(element["id"])
        } else if (Object .keys(element).length===1 &&element["@id"]) {
          unParsedIDs.push(element["@id"])
        } else {
          throw new UnprocessableEntityException("Object " + JSON.stringify(element) + " cannot be parsed by this service")
        }
      })
      unParsedIDs.map(async (x) => {
        resolutionPromises.push(this.service.resolveURI(x))
      })

      let result= await Promise.all(resolutionPromises)
      transformedPayload = transformedPayload.concat(...result)
      return transformedPayload
    } catch(e) {
      this.logger.log("logged error is e")
      throw (e)
    }
    
  }

}





