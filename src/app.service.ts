import { ForbiddenException, Injectable, Logger, NotFoundException, UnprocessableEntityException } from '@nestjs/common';
import { CredentialSubjectDto, RulesDescription, testResult,VerifiableCredentialDto, opaResult } from './dto';
import { firstValueFrom } from 'rxjs';
import { get_rules, get_all_rules, getId } from './utils';
import { HttpService } from '@nestjs/axios';
import { catchError, throwError } from 'rxjs';
import { ProofService } from './proof.service';
import { json } from 'body-parser';
const  logger = new Logger("MainService")

@Injectable()
export class AppService {
  constructor(private readonly httpService: HttpService) {}
  registryURL = process.env.REGISTRY_URL || "https://registry.abc-federation.dev.gaiax.ovh"
  proofService = new ProofService
  getAllRules():RulesDescription[] {
    return get_all_rules()
  }


  async checkRules( payload: VerifiableCredentialDto<CredentialSubjectDto>[] | any):Promise<any>{
    let rules = get_rules()
    let testResults: any[] = []
    let signatureResults: opaResult[] = []
    let consistencyResults: opaResult[] = []
    let mergedResult: any [] = []
    let promises: Promise<opaResult[]>[] = []
    let consistencyPromises:Promise<any>[] = []
    let signaturePromises:Promise<opaResult>[] = []
    logger.log(`Applying rules ${rules} on VP`)
    payload.map(async vc => {
      signaturePromises.push(this.proofService.validate(vc))
      consistencyPromises.push(this.checkConsistency(vc))

      await Promise.all(consistencyPromises)
      let opaPayload = {
        vc: vc,
        rules:[]
      }
      rules.map(async rule => {
        opaPayload.rules.push(rule.id)
      })
      
      promises.push(this.sendToOPA(opaPayload))
    });
    signatureResults = await Promise.all(signaturePromises)
    consistencyResults = await Promise.all(consistencyPromises)
    testResults = await Promise.all(promises)
    for(let i=0; i<testResults.length; i++) {
      testResults[i].push(signatureResults[i])
      testResults[i].push(consistencyResults[i])
      let responseObject:testResult = {
        vc_id:getId(payload[i]),
        is_valid:true
      }
      responseObject =await this.reformatResult(responseObject, testResults[i],  rules, i, payload)
      mergedResult.push(responseObject)
      
    }
      
    mergedResult.map(x => {
      if(x.is_valid ===false) {
        throw new ForbiddenException(mergedResult)
      }
    })
    
    return mergedResult
}


async resolveURI(uri:string): Promise<{}> {
  if(uri.startsWith("did")) {
    return await this.resolveDid(uri)
  }
  else {
    return await this.getURI(uri)
  }
}

async checkConsistency(vc:VerifiableCredentialDto<CredentialSubjectDto>):Promise<opaResult> {
  let derefencableVC = await this.resolveURI(getId(vc))
  if(!(this.proofService.sha256( await this.proofService.normalize(derefencableVC))===this.proofService.sha256(await this.proofService.normalize(vc)))) {
    return {
      message:"Credential of id " + getId(vc) + "has been tempered",
      result: false,
      rule_id: 'Consistency Rule'
    }
  } else {
    return {
      message:"Credential has not been tempered",
      result: true,
      rule_id: 'Consistency Rule'
    }
  }
}

  private async reformatResult(vcResult: any, testResults:any[],rules:any[],index:number,  payload: VerifiableCredentialDto<CredentialSubjectDto>[] ):Promise<any>{
    let subsequentOpa = {
      vc: payload[index],
      rules:[]
    }
    testResults.map(result => {
      if(result.rule_id==="Signature Rule" || result.rule_id==="Consistency Rule") {
         if(result.result ==false) {
          if(!vcResult.invalidRules) {
            vcResult.invalidRules = []
          }
          vcResult.is_valid=false
          vcResult.invalidRules.push({
            rule_id:result.rule_id,
            message:result.message
          })
         }
      } else {
        let definedRule = this.getRules(result.rule_id)
      if(definedRule.subsequentRule && result.result == false) {
        if(!vcResult.warning) {
          vcResult.warning = []
        }
        vcResult.warning.push({
          rule_id:definedRule.id,
          message:definedRule.warning
        })
      } else  if(definedRule.subsequentRule && result.result == true) {
        definedRule.subsequentRule.map(rule => {
          subsequentOpa.rules.push(rule.id)
        })
        

      } else if(!definedRule.subsequentRule && result.result==false) {
        if(!vcResult.invalidRules) {
          vcResult.invalidRules = []
        }
        vcResult.is_valid=false
        vcResult.invalidRules.push({
          rule_id:result.rule_id,
          message:definedRule.error
        })
      }
    }
    
      })
      if(subsequentOpa.rules.length>0) {
        let subsequentResult = await this.sendToOPA(subsequentOpa)
        vcResult = this.reformatResult(vcResult,subsequentResult,rules,index,payload)
      }
    
    return vcResult
  }


  private async sendToOPA(payload:any): Promise<opaResult[]> {
    let request = await firstValueFrom(this.httpService.post("http://localhost:8181/v1/data/gaiax/core/abc_checker", {input: payload}))
    return request.data.result
  }


  private async getURI(uri:string):Promise<any> {
    try {
      let resp = await firstValueFrom(this.httpService.get(uri))
      return resp.data
    } catch(e) {
      logger.error(`URI : ${uri} is not resolvable`)
      throw new NotFoundException(`URI : ${uri} is not resolvable`)
    }

  }


  private async resolveDid(did:string):Promise<{}> {
    if (did.startsWith("did:web")) {
      try{
        let doc= await this.getURI(this.parseDidWeb(did))
        return doc
      } catch(e) {
        if(e.response.statusCode === 404) {
          throw new NotFoundException("Did " + did + " is not resolvable")
        }
    
      }
      
    }
    else throw new UnprocessableEntityException(`Did method not supported yet for did ${did} ` )
  }



  private parseDidWeb(uri:string):string {
    const splitted = uri.split(':')
    let url = 'https://'
    for (let i = 2; i < splitted.length; i++) {
      url = url + splitted[i] + '/'
    }
    if (splitted.length == 3) {
      url = url + '.well-known/did.json'
    } else {
      if (!splitted[splitted.length - 1].includes('.json')) {
        url = url + 'did.json'
      }
    }

    return url
  }





  getRules(id:string):RulesDescription {
    let rules = get_rules()
    for(let i=0; i<rules.length;i++) {
      if(rules[i].id == id) {
        return rules[i]
      } else if(rules[i] && rules[i].subsequentRule) {
        for(let y=0; y<rules[i].subsequentRule.length;y++) {
          if(rules[i].subsequentRule[y].id===id) {
            return rules[i].subsequentRule[y]
          }
        }
      }
    }
    throw new NotFoundException("error when querring rules")
    
    
  }

}

  


