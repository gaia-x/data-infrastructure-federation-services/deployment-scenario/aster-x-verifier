FROM node:16.14-alpine@sha256:28bed508446db2ee028d08e76fb47b935defa26a84986ca050d2596ea67fd506 as production-build-stage

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install 
COPY . .
RUN npm run build

EXPOSE 3003
CMD ["node", "dist/main"]