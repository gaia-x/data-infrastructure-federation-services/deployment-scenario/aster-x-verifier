<h1 align="center">Aster-X Verifier</h1>



## Description

This project is a component that can be used to verify the status of a Verifiable Credential. It revolves arround the usage of [Open-Policy-Agent](https://www.openpolicyagent.org/). One basic rule is natively implemented in Typescript which is the signature verification as OPA does not provide mature enough tools to enable this use case. The other rules are to be implemented.

The core functionnality of this component is to apply rules to a set of Verifiable Credential to check its validity within the ecosystem. Therefore, future ecosystems will needs to create rules in OPA depending on the kind of verification they wants to set for their ecosystem. Example of rules could be for example checking if the IssuanceDate is ulterior to the current time, implementing lifecycle verification mecanism or schema rules.

Please note that this project is a prototype and may be subject to architectural modification and is not considered as a production ready component 

### Manage your own Rules

To add or manage your Rules, you will need to specify the route of your rules repository (or the volume associated). In this repository, you will need to create a file named rules.json such as :
```json
[{
    "id":"rule_X",
    "description":"some description",
    "error":"Some error message you wish to return if your rule return false"
},
{
    "id":"rule_Y",
    "description":"some description",
    "subsequentRule":[{
        "id":"rule_Z",
        "description":"some description",
        "error":"Some error message you wish to return if your rule return false"
    }],
    "warning":"Some warnings you want to raise if you use optionnal rules"
}
]
```

This components use recursivity to implements a system of mandatory and non mandatory rules (such as for example rules applied to a specific field that is optionnal). To end a tree branch, it is needed to have no further subsequent rules and an error message defined in the "error" field. It is needed to include a warning field for each non-final branch of the tree that may be used to display information for the user using this component.
## Installation

### Prerequisite

To use this project, you will need to have a OPA server running on localhost (on your machine or as a side car in an infrastructure). To recreate the demonstrator of GXFSFR, you may use [OPAL](https://docs.opal.ac/) and set your data source to [this repository](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/abc-checker)

## Environment

To use the application, you will need to bootstrap an environment variable RULES_PATH. This allow the component to load which rules you defined and send them to your OPA instance

## Local Development

```bash
$ yarn install
```

## Running the app

```bash
# development
$ yarn run start

# watch mode
$ yarn run start:dev

# production mode
$ yarn run start:prod
```

## Test

```bash
# unit tests
$ yarn run test

# e2e tests
$ yarn run test:e2e

# test coverage
$ yarn run test:cov
```



## Use the Verifier

A swagger is avalaible using the `/docs` route

### Get all rules

Using the `/api/rules` route, you can get all current rules you defined previously as mandatory


**Responses:**

If rules are found , a HTTP response status code `200` and a response object will be returned.

**Response object:**

HTTP response status code `200`:

```json
[
  {
    "id": "rule_13",
    "description": "Validate the existence of expirancy",
    "subsequentRule": [
      {
        "id": "rule_14",
        "description": "Validate if the VC is not expired",
        "error": "VC is expired"
      }
    ],
    "warning": "Missing field: expirationDate"
  },
  {
    "id": "rule_15",
    "description": "Validate the existence of credentialStatus List",
    "subsequentRule": [
      {
        "id": "rule_16",
        "description": "Validate if the VC is expired or revoked",
        "error": "VC is revoked or suspended"
      }
    ],
    "warning": "Credential Status cannot be verified to the lack of field : CredentialStatus"
  }
]
]
```


### Get a specific rule

Using the `/api/rules/{id}` route, you can get a specific rule. The parameter id is corresponding to the id field of a rule you defined in your rules files.


**Responses:**

If a rules is found , a HTTP response status code `200` and a response object will be returned.

If none, a HTTP response status code `404` will be returned

**Response object:**

HTTP response status code `200`:

```json

  {
        "id": "rule_14",
        "description": "Validate if the VC is not expired",
        "error": "VC is expired"
  }

```

### Verify

Using the  POST `/api/rules` route, you can apply a set of verification defined by your rules.json to a set of VC. The set of VC can be send as complete JSON object or using their id (or @id as they are similar in jsonld), as the components implements also an ID resolver (supported ecosystem are : http, did:web)


**Responses:**

If all the Vcs are valid, a HTTP response status code `200` and a response object will be returned.

If one or more isn't , a HTTP response status code `403` will be returned as well as a response object

**Response object Success:**

HTTP response status code `200`:

```json
[
  {
    "vc_id": "did:web:gigas.provider.demo23.gxfs.fr:participant:10f73b0bf81e10616bd3ac78c0f77c34c9a6022353ee0077b6e09e87d33ffb9e/vc/5afc855ad63bce0cbd661252abe1d12de57b207902104c673849c2b9cd226485/data.json",
    "is_valid": true,
    "warning": [
      {
        "rule_id": "rule_13",
        "message": "Missing field: expirationDate"
      },
      {
        "rule_id": "rule_15",
        "message": "Credential Status cannot be verified to the lack of field : CredentialStatus"
      }
    ]
  },
  {
    "vc_id": "https://compliance.aster-x.demo23.gxfs.fr/credential-offers/07fc91bd-af9c-4524-8e21-f01e3535b58e",
    "is_valid": true
  },
  {
    "vc_id": "did:web:gigas.provider.demo23.gxfs.fr:participant:10f73b0bf81e10616bd3ac78c0f77c34c9a6022353ee0077b6e09e87d33ffb9e/vc/legal-participant/legal-participant.json",
    "is_valid": true,
    "warning": [
      {
        "rule_id": "rule_13",
        "message": "Missing field: expirationDate"
      },
      {
        "rule_id": "rule_15",
        "message": "Credential Status cannot be verified to the lack of field : CredentialStatus"
      }
    ]
  },
  {
    "vc_id": "did:web:gigas.provider.demo23.gxfs.fr:participant:10f73b0bf81e10616bd3ac78c0f77c34c9a6022353ee0077b6e09e87d33ffb9e/vc/5afc855ad63bce0cbd661252abe1d12de57b207902104c673849c2b9cd226485/gaiax-compliance-service-offering.json",
    "is_valid": true,
    "warning": [
      {
        "rule_id": "rule_15",
        "message": "Credential Status cannot be verified to the lack of field : CredentialStatus"
      }
    ]
  }
]

```

**Response object Fail:**

HTTP response status code `403`:

```json
{
  "statusCode": 403,
  "message": [
    {
      "vc_id": "did:web:gigas.provider.demo23.gxfs.fr:participant:10f73b0bf81e10616bd3ac78c0f77c34c9a6022353ee0077b6e09e87d33ffb9e/vc/5afc855ad63bce0cbd661252abe1d12de57b207902104c673849c2b9cd226485/data.json",
      "is_valid": true,
      "warning": [
        {
          "rule_id": "rule_13",
          "message": "Missing field: expirationDate"
        },
        {
          "rule_id": "rule_15",
          "message": "Credential Status cannot be verified to the lack of field : CredentialStatus"
        }
      ]
    },
    {
      "vc_id": "https://compliance.aster-x.demo23.gxfs.fr/credential-offers/07fc91bd-af9c-4524-8e21-f01e3535b58e",
      "is_valid": false,
      "invalidRules": [
        {
          "rule_id": "rule_12",
          "message": "Signature is invalid"
        },
        {
          "rule_id": "rule_14",
          "message": "VC is expired"
        }
      ]
    },
    {
      "vc_id": "did:web:gigas.provider.demo23.gxfs.fr:participant:10f73b0bf81e10616bd3ac78c0f77c34c9a6022353ee0077b6e09e87d33ffb9e/vc/legal-participant/legal-participant.json",
      "is_valid": true,
      "warning": [
        {
          "rule_id": "rule_13",
          "message": "Missing field: expirationDate"
        },
        {
          "rule_id": "rule_15",
          "message": "Credential Status cannot be verified to the lack of field : CredentialStatus"
        }
      ]
    },
    {
      "vc_id": "did:web:gigas.provider.demo23.gxfs.fr:participant:10f73b0bf81e10616bd3ac78c0f77c34c9a6022353ee0077b6e09e87d33ffb9e/vc/5afc855ad63bce0cbd661252abe1d12de57b207902104c673849c2b9cd226485/gaiax-compliance-service-offering.json",
      "is_valid": true,
      "warning": [
        {
          "rule_id": "rule_15",
          "message": "Credential Status cannot be verified to the lack of field : CredentialStatus"
        }
      ]
    }
  ],
  "error": "Forbidden"
}


```

### Resolve an ID

Using the `/api/resolve/{id}` route, you can resolve the id of a json object. The resolver support traditionnal URI as well as the did:web method. The resolver also support the addition of a specific path to the did web which allow the dereferencing of object in the did web format (such as did:web:example:participant/data.json). Only the path section of the DID-URL specification is implemented, therefore, sending a DID-URL using query or fragment will results in an error 


**Responses:**

If the ID is resolvable, a HTTP response status code `200` and a response object will be returned.

If none, a HTTP response status code `404` will be returned

**Response object:**

HTTP response status code `200`:

```json

 {
    "foo":"bar",
    "id":"did:web:example:participant/data.josn"
 }

```





